# PR-Booster for Bitbucket - Documentation #

A paid add-on for Bitbucket - [marketplace.atlassian.com/plugins/com.bit-booster.bb](https://marketplace.atlassian.com/apps/1214545/pr-booster-for-bitbucket-server?hosting=server&tab=overview)

## What is this site? ##

This site contains our documentation, our project bug tracker, and some test repos (for testing the plugin).  

## What does the plugin do? ##

PR-Booster adds 3 buttons to the Pull Request screen:

1. **Rebase Pull Request** - Rebases the *from* branch on top of the *to* branch.  Rewrites history of *from* branch with rebased result ([screenshot](https://bit-booster.com/screens/rebase.png "PR-Booster Rebase Screenshot" target="_blank")).

2. **Squash Pull Request** - Squashes the *from* branch (including all merges) into a single commit. Invites users to adjust author and commit message of the final squashed commit. Rewrites history of *from* branch with the squashed result ([screenshot](https://bit-booster.com/screens/squash.png "PR-Booster Squash Screenshot" target="_blank")).

3. **Amend Pull Request** - Allows users to adjust author and commit message on the tip commit of the *from* branch.  Rewrites history of *from* branch with the updated commit. Convenient when merge checks complain about invalid commit messages, or when a previous squash accidentally set the author incorrectly ([screenshot](https://bit-booster.com/screens/amend.png "PR-Booster Amend Screenshot" target="_blank")).

PR-Booster adds 2 buttons to the Commit screen:

1. **Cherry Pick** - Allows users to cherry-pick the current commit over to a different branch (via "git cherry-pick").  Optionally supports a pull-request workflow ([screenshot](https://bit-booster.com/screens/cherry-pick-merge.png "PR-Booster Cherry-Pick Screenshot" target="_blank")).

2. **Revert Commit** - Allows users to create a reverting commit to a given branch (via "git revert").  Optionally supports a pull-request workflow ([screenshot](https://bit-booster.com/screens/revert-merge.png "PR-Booster Revert Screenshot" target="_blank")).

**DEPRECATED** - PR-Booster adds three commit graphs to your instance:

1. **Integrated commit graph** - commit graph integrated into Bitbucket's built-in commits screen ([screenshot](https://bit-booster.com/screens/bitbucket-yes-bit-booster.png "PR-Booster Integrated Commits Screenshot" target="_blank")).

2. **All Branches Graph** - a separate page provided by PR-Booster that let's users see all branches at once ([screenshot](https://bit-booster.com/screens/all-branches.png "PR-Booster All Branches Graph Screenshot" target="_blank")).

3. **Condensed Filtered Git Graph** - a special mode of the **All Branches Graph** that filters out commits based on search criteria ([screenshot](https://bit-booster.com/screens/all-branches-condensed-graph.png "PR-Booster All Branches Condensed Graph Screenshot" target="_blank")).

**DEPRECATED - These 3 graphs are transitioning to our free [Commit Graph for Bitbucket Server](https://marketplace.atlassian.com/apps/1219256/commit-graph-for-bitbucket-server?hosting=server&tab=overview) app.**

## How do I set it up? ##

- **DEPRECATED - All Branches Commit Graph**, **DEPRECATED - Commit Graph**, and **DEPRECATED - Reverted Commits** are automatically enabled for all repositories the moment PR-Booster is installed.  

- **Rebase / Squash / Amend** buttons are automatically enabled for all pull-requests.  They can be enabled/disabled globally, per-project, or per-repository.  If your PR-Booster license becomes invalid, your Bitbucket instance will be limited to 10 rebases / squashes per day.

- **Cherry-Pick / Revert** buttons are also automatically enabled for all repositories.  They can also be enabled/disabled globally, per-project, or per-repository.  If your PR-Booster license becomes invalid, your Bitbucket instance will be limited to 10 cherry-picks / reverts per day.

- **DEPRECATED - PR-Booster Hook** and **DEPRECATED - PR-Booster - Require Fast-Forwards Hook** appear in the "Hooks" section of your repositories (or "Merge Checks" if on Bitbucket 5.2.x or newer).  They are "disabled" by default, and you must click "enable" inside each repository (or each project as of Bitbucket 5.2.x) to activate these hooks for said repository.  The PR-Booster hooks are not affected by license status, and remain fully functional even if your license becomes invalid.

- **Evaluation License Expiry** - Most features remain functional forever even if customers decide to not purchase a paid license for the add-on.  The add-on also stays relatively quiet if the evaluation license expires, and tries not to annoy your users too much.  (It only mentions the expired evaluation license whenever it blocks a foxtrot merge during a push or a merge, which are relatively rare occurrences in most shops, or when users click on the Rebase / Squash / Amend buttons).  Note: paid licenses are perpetual and never expire.

## Why are the hooks deprecated? ##

The foxtrot and fast-forward hooks are transitioning to our free [Control Freak for Bitbucket Server](https://marketplace.atlassian.com/apps/1217635/control-freak-for-bitbucket-server?hosting=server&tab=overview) app.

## Who do I talk to? ##

* For technical support and general questions about the plugin, please email support directly: [support@bit-booster.com](mailto:support@bit-booster.com).
  
* For consulting work, please email me directly (sylvie@bit-booster.com).


## FAQ ##

### Rebasing and Squashing rewrites history.  Isn't this bad? ###

Rewriting history on short-lived feature branches is a common practice in shops that follow a rebase workflow. The resulting git history tends to be more linear and easier to analyze and understand.

If a short-lived branch has several collaborators working together on it, a rebase workflow can get awkward. As long as everyone on your team uses "git pull --rebase" rather than default "git pull", the awkwardness can be significantly reduced (a non-intuitive remedy, but it works).

You should never rewrite history on long-lived branches (e.g., 'master' or 'release/\*\*').  Our Rebase, Squash, and Amend functionality behaves like a regular git client performing "git push --force-with-lease", and so any Bitbucket branch permissions in place to prevent history rewrites will block the push.  We also probe the branch permissions and gray out / disable our buttons if history rewrites are disallowed for the *from* branch.

### The Squash and Amend dialogs can adjust the commit author.  Isn't this potentially dangerous? ###

Since a squash combines several commits into one, it's important to let users decide on which author (from the several commits) best represents the authorship of the work as a whole.  And since user-error is possible when making this decision, it's also important to give users a way to fix any mistakes, hence the author dropdown on the Amend dialog.

The dropdown only contains the names of authors from commits associated with the pull-request. This means the dropdown typically only contains 2-3 possible author values.

The GIT_COMMITTER_NAME and GIT_COMMITTER_EMAIL values in the commit are set to the current user, and so if any mischief is caused through this feature, the culprit is easy to find (git log --pretty=fuller).

It's also worth noting that users can directly control the author metadata in the commits they create by setting GIT_AUTHOR_NAME and GIT_AUTHOR_EMAIL environment variables on their own computers.

### Is PR-Booster really the best commit graph on Bitbucket Server? ###

Take a look at our analysis for yourself and see if you agree: [bit-booster.com/best.html](http://bit-booster.com/best.html).